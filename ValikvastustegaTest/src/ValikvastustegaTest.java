
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ValikvastustegaTest {
	
	static List<String> k�simused = new ArrayList<String>();
	static List<String> vastusevariandid = new ArrayList<String>();
	static List<String> oigedVastused = new ArrayList<String>();
	static int k�simusNo;
	
	public static void failistLugemine(String failinimi) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File(failinimi))));
			String rida;
			while((rida = br.readLine())!=null) {
				String[] t�kid = rida.split(" ; ");
				if (t�kid.length < 3) {
					k�simused.add(t�kid[0]);
					vastusevariandid.add("");
					oigedVastused.add(t�kid[1]);
				} else {
					k�simused.add(t�kid[0]);
					vastusevariandid.add(t�kid[1]);
					oigedVastused.add(t�kid[2]);
				}
			}
				br.close();
		} catch (IOException e) {
			System.out.println("Sellise nimega faili ei ole.");
		}		
	}
	
	public static String[] vastusteS
	
	public static String vastuseKontroll (String kasutajaVastus) {
		if (oigedVastused.get(k�simusNo).equals(kasutajaVastus)) {
			return "Vastus on �ige";
		} else {
			return "Vastus on vale"; 
		}
	}
	
	public static void v�ljastaK�simus() {
    	k�simusNo = (int) (Math.random() * (k�simused.size()-1) + 0);
    	System.out.println(k�simused.get(k�simusNo));
	}
    
    public static void main(String[] args) {
    	failistLugemine("k�simused.txt");
    	String kasutajaVastus="";
    	v�ljastaK�simus();
    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    	try {
			kasutajaVastus = br.readLine();
			br.close();
		} catch (IOException e) {
			System.out.println("Vastust ei sisestatud");
		}
    	System.out.println(vastuseKontroll(kasutajaVastus));
	}	
}