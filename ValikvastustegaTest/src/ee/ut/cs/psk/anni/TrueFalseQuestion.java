package ee.ut.cs.psk.anni;

public class TrueFalseQuestion extends Question {

	public TrueFalseQuestion(String question, String correctAnswer) {
		super();
		this.question=question;
		this.correctAnswer=correctAnswer;
	}

	@Override
	public StringBuilder getHtml() {
		StringBuilder TFQhtml = new StringBuilder();
		TFQhtml.append("<h2>"+question+"</h2>\r\n");
		TFQhtml.append("<input type=\"radio\" name=\""+questionNo+" value=\"jah\">jah\r\n");
		TFQhtml.append("<input type=\"radio\" name=\""+questionNo+" value=\"ei\">ei\r\n");

		return TFQhtml;

	}

}

