package ee.ut.cs.psk.anni;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.*;

public class ValikvastustegaTest{
	
	static List<Question> k�simused = new ArrayList<Question>();
	static int k�simusNo;
	
	public static void failistLugemine(String failinimi) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					new FileInputStream(new File(failinimi))));
			String rida;
			while((rida = br.readLine())!=null) {
				String[] t�kid = rida.split(" ; ");
				if (t�kid.length < 3) {
					k�simused.add(new ShortAnswerQuestion(t�kid[0], t�kid[1]));
				} else if (t�kid[1].equals("")) {
					k�simused.add(new TrueFalseQuestion(t�kid[0],t�kid[2]));
				} else {
					k�simused.add(new MultipleChoiceQuestion(t�kid[0], t�kid[1], t�kid[2]));
				}
			}
				br.close();
		} catch (IOException e) {
			System.out.println("Sellise nimega faili ei ole.");
		}		
	}
	
	public static void vastuseKontroll (String kasutajaVastus) {
		if (k�simused.get(k�simusNo).correctAnswer.equals(kasutajaVastus)) {
			System.out.println("Vastus on �ige");
		} else {
			System.out.println("Vastus on vale"); 
		}
	}
	
	public static String variandidToString(MultipleChoiceQuestion k�simus) {
		List<String> a = new ArrayList<String>(Arrays.asList((k�simus.choices).split("/")));
		return "1. "+a.get(0)+" 2. "+a.get(1)+" 3. "+a.get(2);
	}
	
	public static void v�ljastaK�simus() {
		System.out.println((k�simused.get(k�simusNo)).question);
		if (k�simused.get(k�simusNo) instanceof MultipleChoiceQuestion) {
			System.out.println(variandidToString((MultipleChoiceQuestion)k�simused.get(k�simusNo)));
		}
	}
	public static void kirjutaHtml (StringBuilder sb, String failinimi) {
		String html = sb.toString();
		Writer out;
		try {
			out = new FileWriter(new File(failinimi), true);
			out.write(html);
			out.close();
		} catch (Exception e) {
			System.out.println("Tekkis probleem html-i kirjutamisel: "+e);
		}
	}
	
	public static String getKasutajaVastus() {
		String kasutajaVastus = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(
				System.in));
		try {
			kasutajaVastus = br.readLine();
			br.close();
		} catch (IOException e) {
			System.out.println("Vastust ei sisestatud");
		}
		return kasutajaVastus;
	}
    
	public static void main(String[] args) {
		failistLugemine(args[0]);
		v�ljastaK�simus();
		vastuseKontroll(getKasutajaVastus());
		if (args.length > 1) {
			kirjutaHtml((k�simused.get(k�simusNo)).getHtml(), args[1]);
		}

	}	
}