package ee.ut.cs.psk.anni;

abstract class Question {
	
	public String question;
	public String correctAnswer;
	public StringBuilder html;
	public int questionNo;

	public abstract StringBuilder getHtml();
}

public class MultipleChoiceQuestion extends Question {

	public String choices;
	public MultipleChoiceQuestion(String question, String choices, String correctAnswer) {
		super();
		this.question=question;
		this.choices=choices;
		this.correctAnswer=correctAnswer;
	}
	
	public StringBuilder getHtml() {
		String[] a = choices.split("/");
		StringBuilder MCQhtml = new StringBuilder();
		MCQhtml.append("\r\n<h2>"+question+"</h2>\r\n");
		MCQhtml.append("<select>\r\n");
		MCQhtml.append("<option></option>\r\n");
		for (String choice : a) {
			MCQhtml.append("<option>"+choice+"</option>\r\n");
		}
		MCQhtml.append("</select>\r\n");

		return MCQhtml;
	}
}
