package ee.ut.cs.psk.anni;

public class ShortAnswerQuestion extends Question {
	
	public ShortAnswerQuestion(String question, String correctAnswer) {
		super();
		this.question=question;
		this.correctAnswer=correctAnswer;
	}

	@Override
	public StringBuilder getHtml() {
		StringBuilder SAQhtml = new StringBuilder();
		SAQhtml.append("<h2>"+question+"</h2>\r\n");
		SAQhtml.append("<input/>\r\n");

		return SAQhtml;
	}
}
